open util/ordering [State]

abstract sig Person {
	speed: Int
}

one sig Indiana, GirlFriend, Father, FatherInLaw,  FlashLight extends Person {}


fact speeds {
	Indiana.speed = 1
	GirlFriend.speed = 2
	Father.speed = 4
	FatherInLaw.speed = 5
	 FlashLight.speed = 0
}

sig State {
	near: set Person,
	far: set Person,
	time: Int
}


fact InitialState {
	let s0 = first[] | (s0.near = Person) and (no s0.far) and (s0.time = 0)
}

fun max [speed1, speed2: Int]: Int {
	(speed1 < speed2) implies speed2 else speed1
}

pred crossBridge[from, from', to, to': Person, time, time': Int] {
	some person1, person2: from - FlashLight | {
		from' = from - FlashLight - person1 - person2
		to' = to + FlashLight + person1 + person2
		time' = plus[time, max[person1.speed, person2.speed]]
	}
}

fact traces {
	all s: State, s': next[s] | {
		(FlashLight in s.near implies crossBridge[s.near, s'.near, s.far, s'.far, s.time, s'.time])
		and
		(FlashLight in s.far implies crossBridge[s.far, s'.far, s.near, s'.near, s.time, s'.time])
	}
}

pred Solve {
	let sN = last[] | (no sN.near)  and (sN.far = Person) and (sN.time <= 12)
}

run Solve for 6 but 7 Int
