sig Node {}

sig Graph {
	edge: Node -> Node
}

pred Aciclico [g: Graph] {
	no (iden & ^(g.edge))
}

pred NoDirigido [g:Graph] {
	g.edge = ~(g.edge)
}

pred FuertementeConexo [g: Graph] {
	^(g.edge) = Node->Node
}

pred Conexo [g: Graph] {
	^(g.edge + ~g.edge) = Node->Node
}

pred ComponenteFuertementeConexa [g: Grap] {
	some g': Graph | g'.edge in g.edge and FuertementeConexo[g']
}

pred ComponenteConexa [g: Grap] {
	some g': Graph | g'.edge in g.edge and Conexo[g']	
}
