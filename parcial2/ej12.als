open util/ ordering[Book]

abstract sig Target { }
sig Addr extends Target { }
abstract sig Name extends Target { }

sig Alias, Group extends Name { }

sig Book {
	names: set Name,
	addr: names->some Target
} {
	no n: Name | n in n.^addr
	all a: Alias | lone a.addr
}

pred add [b, b': Book, n: Name, t: Target] {
	t in Addr or some lookup [b, Name&t]
	b'.addr = b.addr + n->t
}

pred del [b, b': Book, n: Name, t: Target] {
	no b.addr.n or some n.(b.addr) - t
	b'.addr = b.addr - n->t
}

fun lookup [b: Book, n: Name] : set Addr { n.^(b.addr) & Addr }

fact Traces {
	no first[].addr
	all b: Book, b': next[b] - last[] |
		(some n: Name, a: Addr | add[b, b', n, a])
		or
		(some n: Name | some lookup[b, n])
}


assert BookOK {
	all b, b': Book, n, n': Name, a, a': Addr |
		(n->a) in b.addr and (add[b, b', n', a'] or some lookup[b, n']) implies
			(n->a) in b'.addr
}
check BookOK

