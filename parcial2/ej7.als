sig Interprete {}

sig Cancion {}

sig Catalogo {
	canciones: set Cancion,
	interpretes: set Interprete,
	interpretaciones: canciones -> interpretes
}{
	canciones.interpretaciones = interpretes
	interpretaciones.interpretes = canciones
}


pred add [c, c': Catalogo, s: Cancion, i: Interprete] {
	c'.interpretaciones = c.interpretaciones + s->i
	c'.canciones = c.canciones + s
	c'.interpretes = c.interpretes + i
}

pred del [c, c': Catalogo, s: Cancion, i: Interprete] {
	c'.interpretaciones = c.interpretaciones - s->i
}

fun pares [c: Catalogo]: Interprete->Interprete {
	~(c.interpretaciones).(c.interpretaciones)
}
