open util/ ordering[State]


abstract sig Stake {
	top: Int,
	sizes: Int -> Int
}

one sig s1, s2, s3 extends Stake {}

sig State {
	left: s1,
	center: s2,
	right: s3
}


fact Init {
	let s0 = first[] | 
		(s0.left.top = 4 and (all i: Int | i > 0 and i < 5 and s0.left.sizes[i] = 4 - i + 1))
		and
		(s0.center.top = 0 and s0.center.sizes[0] = 5)
		and
		(s0.right.top = 0  and s0.right.sizes[0] = 5)
}


pred Move [from, from', to, to': Stake] {
	let min = from.sizes[from.top] | {
		min < to.sizes[to.top] implies {
			to'.top = plus[to.top, 1]
			to'.sizes = to.sizes + (to'.top->min)
			from'.top = minus[from.top, 1]
			from'.sizes = from.sizes - (from.top->min)
		}
		else {
			to' = to
			from' = from
		}
	}
}


fact Traces {
	all s: State, s': next[s] |
		(s.left.top > 0  implies
			Move[s.left, s'.left, s.center, s'.center] or Move[s.left, s'.left, s.right, s'.right])
		and
		(s.center.top > 0  implies
			Move[s.center, s'.center, s.left, s'.left] or Move[s.center, s'.center, s.right, s'.right])
		and
		(s.right.top > 0  implies
			Move[s.right, s'.right, s.left, s'.left] or Move[s.right, s'.right, s.center, s'.center])
}

pred Solve {
	let sN = last[] |
		(sN.left.top = 0 and sN.center.top = 0 and sN.right.top = 4)
		or
		(sN.left.top = 0 and sN.center.top = 4 and sN.right.top = 0)
}
run Solve for 10 but 8 Int

