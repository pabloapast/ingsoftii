abstract sig Target { }
sig Addr extends Target { }
abstract sig Name extends Target { }

sig Alias, Group extends Name { }

sig Book {
	names: set Name,
	addr: names->some Target
} {
	no n: Name | n in n.^addr
	all a: Alias | lone a.addr
}

pred add [b, b': Book, n: Name, t: Target] {
	t in Addr or some lookup [b, Name&t]
	b'.addr = b.addr + n->t
}

pred del [b, b': Book, n: Name, t: Target] {
	no b.addr.n or some n.(b.addr) - t
	b'.addr = b.addr - n->t
}

fun lookup [b: Book, n: Name] : set Addr { n.^(b.addr) & Addr }


pred TwoLevels[b: Book] {
	all n: b.names | #((n.(b.addr)).(b.addr)) > 0
//#((Name.(b.addr)).(b.addr)) > 0
}
run TwoLevels

pred NonEmptyGroup[b: Book] {
	some g: Group | some g.(b.addr)
}
run NonEmptyGroup
