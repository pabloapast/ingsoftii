sig StateA, StateB {}

sig GraphA {
	init: one StateA,
	edge: StateA -> StateA,
	tau: StateA -> StateA
}{
	(init->StateA) in ^(edge + tau)
}

sig GraphB {
	init: one StateB,
	edge: StateB -> StateB,
	tau: StateB -> StateB
}{
	(init->StateB) in ^(edge + tau)
}

sig Relation {
	rel: StateA -> StateB
}

pred Simulacion [gA, gB] {

}

pred BiSimulacion [gA, gB] {

}

pred BiSimulacionDebil [gA, gB] {

}















