sig Elem {}

sig BinaryRel {
	map: Elem -> Elem
}


pred Preorden [b: BinaryRel] {
	(iden & Elem->Elem) in b.map  // Reflexiva
	(b.map).(b.map) in (b.map)  // Transitiva
}

pred OrdenParcial [b: BinaryRel] {
	Preorden[b]
	b.map & ~(b.map) in iden  // Antisimetria
}

pred OrdenTotal [b: BinaryRel] {
	OrdenParcial[b]
	b.map + ~(b.map) = Elem->Elem  // Total
}

pred OrdenEstricto [b: BinaryRel] {
	(b.map).(b.map) in (b.map)  // Transitiva	
	not (iden & Elem->Elem) in b.map
	b.map + ~(b.map) = Elem->Elem  // Total	
	b.map & ~(b.map) = none->none  // Antisimetria
}

