sig Addr, Data {}

sig Memory {
	addrs: Addr,
	map: addrs -> one Data
}

sig MainMemory extends Memory {}

sig Cache extends Memory {
	dirty: set addrs
}

sig System {
	cache: Cache,
	main: MainMemory
}


pred Write [s, s': System, d: Data, a: Addr] {
	s'.cache.addrs = s.cache.addrs ++ a
	s'.cache.map = s.cache.map ++ (a->d)
	s'.cache.dirty = s.cache.dirty ++ a
	s'.main = s.main
}

pred Flush [s, s': System] {
	s'.main.map = s.main.map ++ s.cache.map
	s'.cache.dirty = none
	s'.cache.map = s.cache.map
}

pred Load [s, s': System, a: Addr] {
	s'.main.map = s.main.map
	s'.cache.addrs = s.cache.addrs ++ a
	s'.cache.map = s.cache.map ++ (a->(s.main.map[a]))
	s'.cache.dirty = s.cache.dirty ++ a
}

pred Read [s, s': System, d: Data, a: Addr] {
	((not a in s.cache.addrs) implies Load [s, s', a])
	and
	((a in s.cache.addrs) implies (s'.main = s.main and s'.cache = s.cache))
	and
	(d = s.cache.map[a])
}


pred Consistent [s: System] {
	(s.cache.map - ((s.cache.dirty)->Data)) in s.main.map
}


assert WriteOK {
	all s, s': System, a: Addr, d: Data |
		(Consistent[s] and Write[s, s', d, a]) implies Consistent[s']
}
check WriteOK 

assert FlushOK {
	all s, s': System |
		(Consistent[s] and Flush[s, s']) implies Consistent[s']
}
check FlushOK 

assert LoadOK {
	all s, s': System, a: Addr |
		(Consistent[s] and Load[s, s', a]) implies Consistent[s']
}
check LoadOK 

assert ReadOK {
	all s, s': System, a: Addr, d: Data |
		(Consistent[s] and Read[s, s', d, a]) implies Consistent[s']
}
check ReadOK 






