sig Nodo {}

sig Grafo {
	transiciones: Nodo -> Nodo
}{
	not no transiciones
}

pred Es_aciclico [g: Grafo] {
	no(^(g.transiciones) & iden)
}

pred No_dirigido [g: Grafo] {
	g.transiciones = ~(g.transiciones)
}

pred Es_Fuer_conexo [g: Grafo] {
	^(g.transiciones) = Nodo -> Nodo
}

pred Es_conexo [g: Grafo] {
	^(g.transiciones + ~(g.transiciones)) = Nodo -> Nodo
}

pred Comp_Fuert_Conx[g:Grafo] {
	some g2: Grafo | (g2.transiciones in g.transiciones) && Es_Fuer_conexo[g2]
}


pred Comp_Conx[g:Grafo] {
	some g2: Grafo | (g2.transiciones in g.transiciones) && Es_conexo[g2]
}

assert A {
	// todo grafo que tenga 5 transiciones es ciclico.
	all g:Grafo | ((#g.transiciones = 5) => not Es_aciclico[g])
}

assert B {
	// todo grafo que tenga 5 transiciones no es fuertemente conexo.
	all g:Grafo |  ((#g.transiciones = 5) => not Es_Fuer_conexo[g])
}

assert C {
	// todo grafo que tenga 5 transiciones no tiene componenete fuertemente conexo.
	all g:Grafo |  ((#g.transiciones = 5) => not Comp_Fuert_Conx[g])
}

check C for 1 but 6 Nodo

