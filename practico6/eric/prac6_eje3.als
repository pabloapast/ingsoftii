sig Addr, Data {}

sig Memory {
    addrs: set Addr,
    map: addrs -> one Data
}

sig MainMemory extends Memory { }

sig Cache extends Memory {
    dirty: set addrs
}

sig System {
    cache: Cache,
    main: MainMemory
}

pred Consistent[s: System] {
    (s.cache.map - (s.cache.dirty->Data)) in s.main.map
}

pred Flush [s,s': System] {
        s'.main.map = s.main.map ++ (s.cache.map)
        s'.cache.dirty = none
        s'.cache.map = s.cache.map   
}

pred Load [s,s': System, a:Addr] {
	s'.cache.map = s.cache.map ++ (a -> s.main.map[a])
	s'.cache.addrs = s.cache.addrs ++ a
	s'.cache.dirty = s.cache.dirty ++ a
	s'.main.map = s.main.map
}

pred Write [s, s':System, d:Data, a:Addr]{ 
	s'.cache.map = s.cache.map ++ (a -> d)
	s'.cache.addrs = s.cache.addrs ++ a
	s'.cache.dirty = s.cache.dirty ++ a
	s'.main = s.main
}

pred Read[s, s':System,  d:Data, a:Addr]{
	( not (a in s.cache.addrs) => Load[s,s',a] ) && 
	( a in s.cache.addrs => (s'.main = s.main && s'.cache = s.cache) ) && 
	( d = s'.cache.map[a] )
}


assert Check_WriteSys {
	all s, s2: System, a:Addr, d:Data | Consistent [s] && Write[s, s2, d, a] => Consistent[s2]
}

assert Check_ReadSys {
	all s, s2: System, a:Addr, d:Data | Consistent [s] && Read[s, s2, d, a] => Consistent[s2]
}

assert Check_LoadSys {
	all s, s2: System, a:Addr | Consistent [s] && Load[s, s2, a] => Consistent[s2]
}

assert Check_FlushSys {
	all s, s2: System | Consistent [s] && Flush[s, s2] => Consistent[s2]
}



//check Check_WriteSys

check Check_ReadSys

//check Check_LoadSys

//check Check_FlushSys

