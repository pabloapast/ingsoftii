open util/ordering[Estado] 

abstract sig Persona {
	golpea:  set Persona,
	control:  set Persona
}

// se deberia llamar Objetos y no Personas...
one sig Barco, Policia, Preso, Padre, Madre, Hijo1, Hijo2, Hija1, Hija2 extends Persona{}

fact golpear {
	Preso.golpea = Padre + Madre + Hijo1 + Hijo2 + Hija1 + Hija2
	Madre.golpea = Hijo1 + Hijo2
	Padre.golpea = Hija1 + Hija2
	no Policia.golpea
	no Hijo1.golpea
	no Hijo2.golpea
	no Hija1.golpea
	no Hija2.golpea

	no Barco.golpea
}

fact controlar {
	no Preso.control
	Madre.control = Padre
	Padre.control = Madre
	Policia.control = Preso
	no Hijo1.control
	no Hijo2.control
	no Hija1.control
	no Hija2.control

	no Barco.control
}


sig Estado {
	inicio: set Persona, 
	final: set Persona
} 


// Estado inicial
fact EstadoInicial {
	let s0 = first[] | s0.inicio = Persona && no s0.final 
}


// Transiciones
pred CruzarRioMayor [inicio, inicio', final, final': set Persona, mayor: Policia + Padre + Madre] {
	( let inic = inicio - mayor| let fin = final + mayor |
		( inicio' = inic - (inic - (inic .control)).golpea - Barco 
		&& 
		   final' = fin - (fin - (fin .control)).golpea + Barco)
	 )
	||
	( some item: inicio - mayor | let inic = inicio - mayor - item | let fin = final + mayor + item |
		( inicio' = inic - (inic - (inic .control)).golpea - Barco 
		&& 
		   final' = fin - (fin - (fin .control)).golpea + Barco)
	)
}

fact TransicionEstado {
	all s: Estado, s': next[s] | (
		( Barco in s.inicio => 
			(
				( Policia in s.inicio && CruzarRioMayor [ s.inicio, s'.inicio, s.final, s'.final, Policia] )
				||
				( Padre in s.inicio && CruzarRioMayor [ s.inicio, s'.inicio, s.final, s'.final, Padre] )
				||
				( Madre in s.inicio && CruzarRioMayor [ s.inicio, s'.inicio, s.final, s'.final, Madre] )
			)
		)
		&&
		( (not Barco in s.inicio) => 
			(
				( Policia in s.final  && CruzarRioMayor [ s.final, s'.final, s.inicio, s'.inicio, Policia] ) 
				||
				( Padre in s.final  && CruzarRioMayor [ s.final, s'.final, s.inicio, s'.inicio, Padre] )
				||
				( Madre in s.final  && CruzarRioMayor [ s.final, s'.final, s.inicio, s'.inicio, Madre] )
			)
		)

	)
}

// Estado a alcanzar
pred EstadiFinalDeseado[] {
	some s: Estado | s.final = Persona
}


// Ejecución
run EstadiFinalDeseado for 18 Estado












