sig Interprete {}

sig Cancion {}

sig Catalogo {
	canciones: set Cancion,
	interpretes: set Interprete,
	interpretaciones: canciones -> interpretes
}{
	all c: Cancion | c in canciones => (some i: Interprete | (c -> i) in interpretaciones)
	all i: Interprete | i in interpretes => (some c: Cancion | (c -> i) in interpretaciones)
}


pred agregar[catalogo,catalogo': Catalogo, c:Cancion, i:Interprete] {
	catalogo'.interpretaciones = catalogo.interpretaciones ++ (c -> i)
	catalogo'.canciones = catalogo.canciones
	catalogo'.interpretes = catalogo.interpretes
}


pred eliminar[catalogo,catalogo': Catalogo, c:Cancion, i:Interprete] {
	catalogo'.interpretaciones = catalogo.interpretaciones - (c -> i)
	catalogo'.canciones = catalogo.canciones
	catalogo'.interpretes = catalogo.interpretes
}


fun inters [c: Catalogo] : (set Interprete -> set Interprete) { 
	(~(c.interpretaciones)).(c.interpretaciones) - iden
}

// las canciones tienen a lo sumo 1 interprete (deberia fallar....).
assert A {
	all c: Catalogo | no(inters[c])
}

check A for 1 but 3 Cancion, 3 Interprete





