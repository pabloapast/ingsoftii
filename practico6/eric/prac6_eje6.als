sig StateA {}
sig StateB {}

sig SystemA {
	init: one StateA,
	map_a: StateA -> StateA,
	map_tau: StateA -> StateA
}{
	(init.(^(map_a + map_tau))) = StateA
}

sig SystemB {
	init: one StateB,
	map_a: StateB -> StateB,
	map_tau: StateB -> StateB
}{
	(init.(^(map_a + map_tau))) = StateB
}

sig Relacion {
	relac: StateA -> StateB
}

// r es una simulacion de s2 a s
pred es_simulacion[s: SystemA, s2: SystemB, r: Relacion] {
	(~(r.relac)).(s.map_a) in (s2.map_a).(~(r.relac)) &&
	(~(r.relac)).(s.map_tau) in (s2.map_tau).(~(r.relac))
}

// r es una bisimulacion de s2 a s
pred es_bisimulacion[s: SystemA, s2: SystemB, r: Relacion] {
	es_simulacion[s,s2,r] && (((r.relac).(s2.map_tau)) in ((s.map_tau).(r.relac))) && 
	(((r.relac).(s2.map_a)) in ((s.map_a).(r.relac)))
}

// r es una bisimulacion debil de s2 a s
pred es_bisimulacion_debil[s: SystemA, s2: SystemB, r: Relacion] {
	(~(r.relac)).( (*(s.map_tau)).(s.map_a).(*(s.map_tau)) ) in ( (*(s2.map_tau)).(s2.map_a).(*(s2.map_tau)) ).(~(r.relac)) &&
	(r.relac).( (*(s2.map_tau)).(s2.map_a).(*(s2.map_tau)) ) in ( (*(s.map_tau)).(s.map_a).(*(s.map_tau)) ).(r.relac) &&
	(~(r.relac)).(s.map_tau) in (*(s2.map_tau) ).(~(r.relac)) && 	(r.relac).(s2.map_tau) in (*(s.map_tau) ).(r.relac)
}

assert A {
	all s:SystemA, s2:SystemB, r:Relacion | es_bisimulacion[s,s2,r] => es_simulacion[s,s2,r]
}

assert B {
	all s:SystemA, s2:SystemB, r:Relacion | es_bisimulacion[s,s2,r] => es_bisimulacion_debil[s,s2,r]
}

assert C {
	all s:SystemA, s2:SystemB, r, r2, comp:Relacion | (es_bisimulacion_debil[s,s2,r] 
					&& es_bisimulacion_debil[s,s2,r2] 
					&& comp.relac = ((r.relac) + (r2.relac))
					&&  not no((r.relac) - (r2.relac)) ) => not es_bisimulacion_debil[s,s2,comp]
}


check C






