// 4 discos y 3 estacas.
open util/ordering[Estado] 


abstract sig Estaca {
	discos: Int -> Int,
	top: Int
}

one sig EstacaA, EstacaB, EstacaC extends Estaca {}

sig Estado {
	estaca_a: EstacaA, 
	estaca_b: EstacaB, 
	estaca_c: EstacaC, 
}


// Estado inicial
fact EstadoInicial {
	let s0 = first[] | (
		(s0.estaca_a.top = 4) && (s0.estaca_b.top = 0) && (s0.estaca_c.top = 0) 
		 && (all i: Int | i >= 1 && i <=4 => s0.estaca_a.discos[i] = 4 + 1 - i)
	)
}

pred Mover [estaca_in,estaca_in', estaca_out,estaca_out': Estaca] {
	let min = estaca_in.discos[estaca_in.top] |
	(estaca_out.top > 0 => 
		( 
			(
				(min < estaca_out.discos[estaca_out.top]) =>
					(
						estaca_in'.discos = estaca_in.discos - (estaca_in.top->min) &&
						estaca_in'.top = minus[estaca_in.top, 1] &&
						estaca_out'.discos = estaca_out.discos ++ (plus[estaca_out.top, 1]->min) &&
						estaca_out'.top = plus[estaca_out.top, 1]
					)
			)
			&&
			(
				(min >= estaca_out.discos[estaca_out.top]) => 
						( estaca_in' = estaca_in && estaca_out' = estaca_out)
			)

		)
	) 
	&& 
	((estaca_out.top = 0) => ( estaca_in' = estaca_in && estaca_out' = estaca_out))
}


// Transiciones
pred MoverDisco [estaca_a, estaca_a',estaca_b, estaca_b',estaca_c, estaca_c':Estaca] {
	(
		((estaca_a.top > 0) => (Mover[estaca_a,estaca_a',estaca_b,estaca_b'] || Mover[estaca_a,estaca_a',estaca_c,estaca_c']))
		&& 
		((estaca_b.top > 0) => (Mover[estaca_b,estaca_b',estaca_a,estaca_a'] || Mover[estaca_b,estaca_b',estaca_c,estaca_c']))
		&& 
		((estaca_c.top > 0) => (Mover[estaca_c,estaca_c',estaca_b,estaca_b'] || Mover[estaca_c,estaca_c',estaca_a,estaca_a']))
	)
}

fact TransicionEstado {
	all s: Estado, s': next[s] | (
		( Linterna in s.inicio =>  CruzarPuente [s.inicio, s'.inicio, s.final, s'.final, s.tiempo, s'.tiempo] )
		&&
		( (not Linterna in s.inicio) => CruzarPuente [s.final, s'.final, s.inicio, s'.inicio, s.tiempo, s'.tiempo] ) 
	)
}


// Estado a alcanzar
pred EstadiFinalDeseado[] {
	some s: Estado | (
		((s.estaca_a.top = 0) && (s.estaca_b.top = 4) && (s.estaca_c.top = 0))
		||
		((s.estaca_a.top = 0) && (s.estaca_b.top = 0) && (s.estaca_c.top = 4))
	)
}


// Ejecución
run EstadiFinalDeseado for 50 Estado, 8 Int


































