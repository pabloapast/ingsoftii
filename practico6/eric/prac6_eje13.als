sig Elem {}

sig Relacion {
	elems: Elem -> Elem
}{
	not no elems
}

pred SinCiclos[r: Relacion] {
	no(^(r.elems) & iden)
}

pred EsConexo [r: Relacion] {
	^(r.elems + ~(r.elems)) = Elem -> Elem
}

pred UnPadre [r: Relacion] {
	(r.elems).(~(r.elems)) in iden
}

pred EsArbol [r: Relacion] {
	EsConexo[r] && SinCiclos[r] && UnPadre[r]
}


assert A {
	all r: Relacion |  (EsArbol[r] => (#((r.elems).Elem + Elem.(r.elems)) < 4))
}

check A for 1 but 12 Elem

