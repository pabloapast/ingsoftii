open util/ordering[Estado] 


abstract sig Objeto {
	velocidad: Int
}

one sig Linterna, IndianaJones, Novia, Padre, Suegro extends Objeto {}

fact velocidades {
	IndianaJones.velocidad = 1
	Novia.velocidad = 2
	Padre.velocidad = 4
	Suegro.velocidad = 5

	Linterna.velocidad = 0
}

sig Estado {
	inicio: set Objeto, 
	final: set Objeto,
	tiempo: Int
}


// Estado inicial
fact EstadoInicial {
	let s0 = first[] | ((s0.inicio = Objeto) && (no s0.final) && (s0.tiempo = 0))
}

// funcion auxiliar
fun Max[a, b: Int]: Int {
	(a <= b) => b else a
}

// Transiciones
pred CruzarPuente [inicio, inicio', final, final': set Objeto, tiempo,tiempo': Int] {
	some item: inicio - Linterna | some item2: inicio - Linterna |
		( inicio' = inicio - item - item2 - Linterna && final' = final + item + item2 + Linterna && 
		  tiempo' = plus[tiempo, Max[item.velocidad, item2.velocidad]] )
}

fact TransicionEstado {
	all s: Estado, s': next[s] | (
		( Linterna in s.inicio =>  CruzarPuente [s.inicio, s'.inicio, s.final, s'.final, s.tiempo, s'.tiempo] )
		&&
		( (not Linterna in s.inicio) => CruzarPuente [s.final, s'.final, s.inicio, s'.inicio, s.tiempo, s'.tiempo] ) 
	)
}

// Estado a alcanzar
pred EstadiFinalDeseado[] {
	some s: Estado | ((s.final = Objeto) && (s.tiempo <= 12))
}


// Ejecución
run EstadiFinalDeseado for 7 but 8 Int
