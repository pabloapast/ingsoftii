module practico6/ ej9
open util/ ordering[State]


abstract sig Object { 
	speed: Int
}

one sig IndianaJones, GirlFriend, Father, FatherInLaw, FlashLight extends Object {}


fact speeds {
	IndianaJones.speed = 1  // 5
	GirlFriend.speed = 2  // 10
	Father.speed = 4  // 20
	FatherInLaw.speed = 5  // 25
	FlashLight.speed = 0
}


fun max[s1, s2: Int]: Int {
	(s1 < s2) implies s2 else s1
}


sig State {
	east: set Object,
	west: set Object,
	time: Int
}


fact initialState {
	let s0 = first[] | ((s0.east = Object) and (no s0.west) and (s0.time = 0))
}


pred crossBridge[from, from', to, to': set Object, time, time': Int] {
	some x, y: from - FlashLight | {
		from' = from - FlashLight - x - y
		to' = to + FlashLight + x + y
		time' = plus[time, max[x.speed, y.speed]]
	}
}


fact stateTransition {
	all s: State, s': next[s] |
		((FlashLight in s.east) implies crossBridge[s.east, s'.east, s.west, s'.west, s.time, s'.time])
		and
		((FlashLight in s.west) implies crossBridge[s.west, s'.west, s.east, s'.east, s.time, s'.time])
}


pred solvePuzzle {
	(last[].west = Object) and (last[].time <= 12 )
}
run solvePuzzle for 6 but 7 int

